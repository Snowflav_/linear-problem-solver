/* Standard
	Entrée: pb
	Construction de tableau forme standard
	->Variable entrante?
		Non: STOP: Optimum trouvé
		Oui: ->Variable sortante?
			Non: >STOP: Problème non borné
			Oui: Pivotage
	^-------'
*/

/* 2 phases
	Entrée: pb
	-> b >= 0?
		Oui: proceed to standard
		Non: -> UOProblem
*/

public class Problem {
	protected double[] criterion;
	protected double[][] equations;
	protected final int vars;
	protected final int gapVars;
	protected String status;

	public Problem(double[][] tab) 
	throws InvalidProblemException {
		this(tab, true);
	}

	protected Problem(double[][] tab, boolean checkAdmissibleOrigin)
	throws InvalidProblemException {
		if (tab.length == 0)
			throw new InvalidProblemException("Initial array is empty.");
		vars = tab[0].length;
		gapVars = tab.length-1;
		status = "\033[33mUnsolved\033[0m";

		if (gapVars == 0)
			throw new InvalidProblemException("Problem has no constraints, and therefore isn't a problem.");

		criterion = new double[vars + gapVars + 1];
		for (int i = 0; i < vars; i++)
			criterion[i] = tab[0][i];
		for (int i = vars; i < criterion.length; i++)
			criterion[i] = 0;

		equations = new double[vars + gapVars][];
		//Initially, the x variables are in base, so they are not represented
		for (int i = 0; i < vars; i++)
			equations[i] = null;

		//Model the remaining rows after the inequations
		for (int i = vars; i < vars + gapVars; i++) {
			//Check if every inequation has the right amount of parameters
			//ie arrays have the appropriate length
			if (tab[i-vars+1].length != vars +1)
				throw new InvalidProblemException("Inequation " + (i-vars+1) + " has invalid number of parameters");

			//Create subarray for every equation
			equations[i] = new double[vars + gapVars + 1];

			//Add inequation terms
			for (int j = 0; j < vars; j++)
				equations[i][j] = tab[i-vars+1][j];

			//Add identity matrix to its right
			for (int j = vars; j < vars + gapVars; j++)
				equations[i][j] = 0;
			equations[i][i] = 1;

			//Add constraint at the end
			equations[i][vars + gapVars] = tab[i-vars+1][vars];
		}

		//Verify if origin is admissible - Use 2 phase method if not
		if (checkAdmissibleOrigin && !admissibleOrigin()) {
			System.out.println("Origin not admissible in this problem. Proceeding to 2 phase method.");
			UOProblem auxilaryProblem = new UOProblem(makeAuxilaryProblem());
			auxilaryProblem.firstPivot();
			auxilaryProblem.solve();
			if (auxilaryProblem.hasSolutions()) {
				System.out.println("Solutions domain not empty. Reinjecting auxilary problem solutions.");
				auxilaryProblem.reinject(this);
			} else {
				System.out.println("Solutions domain empty. Problem not solvable.");
				status = "\033[31mNo solution\033[0m - Solutions domain empty";
			}
		}
	}

	private boolean admissibleOrigin() {
		for (int i = vars; i < equations.length; i++)
			if (equations[i][vars+gapVars] < 0)
				return(false);
		return(true);
	}

	private double[][] makeAuxilaryProblem() {
		double[][] res = new double[gapVars+1][];

		//Criterion is just -delta
		res[0] = new double[vars+1];
		for (int i = 0; i < vars; i++)
			res[0][i] = 0;
		res[0][vars] = -1;

		//Recreate every other row with -1 added in delta column
		for (int i = 1; i < res.length; i++) {
			res[i] = new double[vars+2];
			for (int j = 0; j < vars; j++)
				res[i][j] = equations[i+vars-1][j];
			res[i][vars] = -1;
			res[i][vars+1] = equations[i+vars-1][vars+gapVars];
		}

		return(res);
	}

	@Override
	public String toString() {
		String res = "   ";
		//First row: showing variable names
		for (int i = 1; i <= vars; i++)
			res += "x" + i + "  ";
		for (int i = 1; i <= gapVars; i++)
			res += "y" + i + "  ";
		res += "\n";

		//Second row: showing variable values
		res += "f  ";
		for (int i = 0; i < criterion.length; i++)
			res += criterion[i] + " ";

		//Third row: line
		res += "\n---";
		for (int i = 0; i < criterion.length; i++)
			res += "---";
		res += "\n";
		
		//Show x variables if they are out of base
		for (int i = 0; i < vars; i++) {
			if (equations[i] != null) {
				res += "x" + (i+1) + " ";
				for (int j = 0; j < equations[i].length; j++)
					res += equations[i][j] + " ";
				res += "\n";
			}
		}

		//Show y variables if they are out of base
		for (int i = vars; i < equations.length; i++) {
			if (equations[i] != null) {
				res += "y" + (i-vars+1) + " ";
				for (int j = 0; j < equations[i].length; j++)
					res += equations[i][j] + " ";
				res += "\n";
			}
		}

		res += "Status: " + status;

		return(res);
	}

	public void tableMethod() {
		int entryVar;
		int exitVar;
		double minRatio;
		int i;
	
		//Find entry variable using Bland's rule
		for (entryVar = 0; criterion[entryVar] <= 0; entryVar++);

		//Find exit variable using Bland's rule
		for (i = 0; equations[i] == null || equations[i][entryVar] <= 0 || equations[i][vars + gapVars] <= 0; i++);
		minRatio = equations[i][vars + gapVars] / equations[i][entryVar];
		exitVar = i;
		for (i = exitVar+1; i < equations.length; i++) {
			if (equations[i] != null && equations[i][entryVar] >= 0 && equations[i][vars + gapVars] >= 0 && equations[i][vars + gapVars] / equations[i][entryVar] < minRatio) {
				minRatio = equations[i][vars + gapVars] / equations[i][entryVar];
				exitVar = i;
			}
		}

		//Divide equations[exitVar] by equations[exitVar][entryVar] to replace the base variables
		{
			double coef = equations[exitVar][entryVar];
			for (i = 0; i < vars + gapVars + 1; i++)
				equations[exitVar][i] /= coef;
		}

		//Subtract equations[l][entryVar]*equations[exitVar] to every line l, except equations[exitVar]
		for (int l = 0; l < exitVar; l++) {
			if (equations[l] != null) {
				double coef = equations[l][entryVar];
				for (i = 0; i < vars + gapVars + 1; i++)
					equations[l][i] = equations[l][i] - coef*equations[exitVar][i];
			}
		}
		for (int l = exitVar + 1; l < equations.length; l++) {
			if (equations[l] != null) {
				double coef = equations[l][entryVar];
				for (i = 0; i < vars + gapVars + 1; i++)
					equations[l][i] = equations[l][i] - coef*equations[exitVar][i];
			}
		}
		//Do the same for the criterion
		double coef = criterion[entryVar];
		for (i = 0; i < criterion.length; i++)
			criterion[i] = criterion[i] - coef*equations[exitVar][i];

		//Replace base variables
		equations[entryVar] = equations[exitVar];
		equations[exitVar] = null;
	}

	public String getCurrentPoint() {
		String res = "(";
		for (int i = 0; i < vars; i++) {
			if (equations[i] != null)
				res += equations[i][vars + gapVars] + ", ";
			else
				res += "0, ";
		}
		res = res.replaceFirst(", $", "): ");
		res += -criterion[vars + gapVars];
		return(res);
	}

	public void solve() {
		while (status == "\033[33mUnsolved\033[0m") {
			//After every iteration check if there are entry and exit variables
			int i;
			for (i = 0; i < criterion.length-1 && criterion[i] <= 0; i++);
			if (i == criterion.length-1)
				status = "\033[32mProblem solved\033[0m - Optimum found at " + getCurrentPoint();
			//TODO check if infinite solutions

			if (status == "\033[33mUnsolved\033[0m") {
				for (i = 0; i < equations.length && (equations[i] == null || equations[i][vars + gapVars] <= 0); i++);
				if (i == equations.length)
					status = "\033[31mNo solution\033[0m - Infinite domain.";
			}

			//Apply table method while there still are entry and exit variables
			if (status == "\033[33mUnsolved\033[0m")
				this.tableMethod();
		}
	}

}
