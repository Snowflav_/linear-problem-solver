public class InvalidProblemException extends Exception {
	public InvalidProblemException(String msg) {
		super(msg);
	}
}
