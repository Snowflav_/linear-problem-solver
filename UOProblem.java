/*
	Prétraîtement phase 1
	Construction pb auxiliaire phase 1
	Nouveau pb avec max(-delta), -1 partout dans colonne delta
	Résourdre pb auxiliaire
	-> delta^x = 0 (critère = 0)?
		Oui: Suppression delta
			 réinjection du problème original dans les variables hors-base
		Non: STOP: Ensemble des contraintes vide
*/

public class UOProblem extends Problem {

	public UOProblem(double[][] tab) 
	throws InvalidProblemException {
		super(tab, false);
	}

	@Override
	public String toString() {
		String res = "   ";
		//First row: showing variable names
		for (int i = 1; i < vars; i++)
			res += "x" + i + "  ";
		res += "δ   ";
		for (int i = 1; i <= gapVars; i++)
			res += "y" + i + "  ";
		res += "\n";

		//Second row: showing variable values
		res += "f  ";
		for (int i = 0; i < criterion.length; i++)
			res += criterion[i] + " ";

		//Third row: line
		res += "\n---";
		for (int i = 0; i < criterion.length; i++)
			res += "---";
		res += "\n";
		
		//Show x variables if they are out of base
		for (int i = 0; i < vars-1; i++) {
			if (equations[i] != null) {
				res += "x" + (i+1) + " ";
				for (int j = 0; j < equations[i].length; j++)
					res += equations[i][j] + " ";
				res += "\n";
			}
		}

		//Show delta if it is out of base
		if (equations[vars-1] != null) {
			res += "δ  ";
			for (int i = 0; i < equations[vars-1].length; i++)
				res += equations[vars-1][i] + " ";
			res += "\n";
		}

		//Show y variables if they are out of base
		for (int i = vars; i < equations.length; i++) {
			if (equations[i] != null) {
				res += "y" + (i-vars+1) + " ";
				for (int j = 0; j < equations[i].length; j++)
					res += equations[i][j] + " ";
				res += "\n";
			}
		}

		res += "Status: " + status;

		return(res);
	}

	public void firstPivot() {
		int entryVar = vars-1;
		int exitVar = vars;

		//Find exit variable
		for (int i = vars+1; i < vars + gapVars; i++)
			if (equations[i][vars + gapVars] < equations[exitVar][vars + gapVars])
				exitVar = i;

		//Divide (or multiply) equations[exitVar] by -1 to replace the base variables
		for (int i = 0; i < vars + gapVars + 1; i++)
			equations[exitVar][i] *= -1;

		//Subtract equations[l][entryVar]*equations[exitVar] to every line l, except equations[exitVar]
		for (int l = 0; l < exitVar; l++) {
			if (equations[l] != null) {
				double coef = equations[l][entryVar];
				for (int i = 0; i < vars + gapVars + 1; i++)
					equations[l][i] = equations[l][i] - coef*equations[exitVar][i];
			}
		}
		for (int l = exitVar + 1; l < equations.length; l++) {
			if (equations[l] != null) {
				double coef = equations[l][entryVar];
				for (int i = 0; i < vars + gapVars + 1; i++)
					equations[l][i] = equations[l][i] - coef*equations[exitVar][i];
			}
		}
		//Do the same for the criterion
		double coef = criterion[entryVar];
		for (int i = 0; i < criterion.length; i++)
			criterion[i] = criterion[i] - coef*equations[exitVar][i];

		//Replace base variables
		equations[vars-1] = equations[exitVar];
		equations[exitVar] = null;
	}

	public void reinject(Problem pb) {
		//Reexpress criterion
		double[] c = new double[pb.criterion.length];
		for (int i = 0; i < c.length; i++)
			c[i] = 0;

		for (int i = 0; i < vars-1; i++) {
			if (equations[i] != null) {
				for (int j = 0; j < i; j++) //Go up to i
					c[j] -= equations[i][j];
				//Skip i
				for (int j = i+1; j < vars-1; j++) //Go up to delta
					c[j] -= equations[i][j];
				//Skip delta
				for (int j = vars; j < equations[i].length; j++)
					c[j-1] -= equations[i][j];
				//c[pb.vars+gapVars] -= equations[i][vars + vapVars];
			} else
				c[i] += pb.criterion[i];
		}
		pb.criterion = c;

		//Reinject auxilary problem rows without the delta column
		for (int i = 0; i < vars-1; i++) {
			if (equations[i] == null)
				pb.equations[i] = null;
			else {
				pb.equations[i] = new double[pb.criterion.length];
				for (int j = 0; j < vars-1; j++)
					pb.equations[i][j] = equations[i][j];
				for (int j = vars; j < equations[i].length; j++)
					pb.equations[i][j-1] = equations[i][j];
			}
		}
		for (int i = vars; i < equations.length; i++) {
			if (equations[i] == null)
				pb.equations[i-1] = null;
			else {
				pb.equations[i-1] = new double[pb.criterion.length];
				for (int j = 0; j < vars-1; j++)
					pb.equations[i-1][j] = equations[i][j];
				for (int j = vars; j < equations[i].length; j++)
					pb.equations[i-1][j-1] = equations[i][j];
			}
		}
	}

	public boolean hasSolutions() {
		return criterion[criterion.length-1] == 0;
	}
}
