public class Test {
	public static void main(String[] args) {
		double[][] td1ex1a = {
			{1, 1d/2d},
			{1, 1, 4},
			{-3, 1, 3},
			{1, -1d/2d, 1}
		};
		double[][] td1ex1b = {
			{10, 20},
			{2, 1, 30},
			{1, 4, 64},
			{5, 6, 110}
		};
		double[][] td1ex1c = {
			{1, 2},
			{-3, 2, 2},
			{-1, 2, 4},
			{1, 1, 5}
		};
		double[][] cours143 = {
			{1, -1},
			{-2, 1, -2},
			{1, -2, 2},
			{1, 1, 5}
		};
		double[][] td1ex6 = {
			{1, 2},
			{2, 3, 12},
			{3, 1, 9},
			{-1, -1, -2}
		};
		double[][] td1ex7 = {
			{1, -2},
			{1, 1, 3},
			{-1, 3, -4}
		};
		Problem pb;
		try {
			pb = new Problem(td1ex7);
			System.out.println(pb);
			pb.solve();
			System.out.println(pb);
		} catch (InvalidProblemException e) {
			System.out.println(e);
		}
	}
}
